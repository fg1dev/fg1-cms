<?php

/** database prefix */
define('DB_PREFIX', '');

/** database type */
define('DB_TYPE', 'mysql');

/** database name */
define('DB_NAME', 'fg1_cms');

/** database username */
define('DB_USER', 'root');

/** database password */
define('DB_USER_PSSWD', '');

/** hostname */
define('DB_HOST', 'localhost');

/** Set locale for date and others stuffs */
setlocale (LC_ALL, 'pt_BR');

/** Set default Timezone */
date_default_timezone_set('America/Sao_Paulo');