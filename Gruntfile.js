/*jslint node: true*/
module.exports = function (grunt) {
  'use strict';

  // Configuração
  grunt.initConfig({

    pkg: grunt.file.readJSON('package.json'),

    uglify: {
      js: {
        options: {
          mangle: false,
          banner: '/*! <%= pkg.name %> - Javascript */\n'
        },
        files: {
          'admin/js/application.min.js' : [
            "admin/js/jquery.js",
            "admin/bs3/js/bootstrap.min.js",
            "admin/js/jquery.dcjqaccordion.2.7.js",
            "admin/js/jquery.scrollTo.min.js",
            "admin/js/jQuery-slimScroll-1.3.0/jquery.slimscroll.js",
            "admin/js/jquery.nicescroll.js",
            "admin/js/easypiechart/jquery.easypiechart.js",
            "admin/js/sparkline/jquery.sparkline.js",
            "admin/js/advanced-datatable/js/jquery.dataTables.js",
            "admin/js/data-tables/DT_bootstrap.js",
            "admin/js/dynamic_table_init.js",
            "admin/js/iCheck/jquery.icheck.js",
            "admin/js/ckeditor/ckeditor.js",
            "admin/js/mini-upload-form/assets/js/jquery.knob.js",
            "admin/js/mini-upload-form/assets/js/jquery.ui.widget.js",
            "admin/js/mini-upload-form/assets/js/jquery.iframe-transport.js",
            "admin/js/mini-upload-form/assets/js/jquery.fileupload.js",
            "admin/js/mini-upload-form/assets/js/script.js",
            "admin/js/bootstrap-switch.js",
            "admin/js/jquery-tags-input/jquery.tagsinput.js",
            "admin/js/jquery.easing.min.js",
            "admin/js/calendar/clndr.js",
            "admin/js/underscore.min.js",
            "admin/js/calendar/moment-2.2.1.js",
            "admin/js/skycons/skycons.js",
            "admin/js/jquery.scrollTo/jquery.scrollTo.js",
            "admin/js/css3clock/js/css3clock.js",
            "admin/js/morris-chart/raphael-min.js",
            "admin/js/jquery.customSelect.min.js",
            "admin/js/jquery.isotope.js",
            "admin/js/select2/select2.js",
            "admin/js/select-init.js",
            "admin/js/scripts.js",
            "admin/js/icheck-init.js",
            "admin/js/ajax.js",
          ]
        }
      }
    },
    cssmin: {
      sitecss: {
        options: {
          banner: '/*! <%= pkg.name %> - Styles */\n'
        },
        files: {
          'admin/css/application.min.css': [
          "admin/bs3/css/bootstrap.min.css",
          "admin/css/bootstrap-reset.css",
          "admin/css/font-awesome.min.css",
          "admin/js/advanced-datatable/css/demo_table.css",
          "admin/js/data-tables/DT_bootstrap.css",
          "admin/js/iCheck/skins/minimal/minimal.css",
          "admin/js/iCheck/skins/square/square.css",
          "admin/js/iCheck/skins/flat/grey.css",
          "admin/js/mini-upload-form/assets/css/bucketmin.css",
          "admin/css/bootstrap-switch.css",
          "admin/js/jquery-tags-input/jquery.tagsinput.css",
          "admin/js/advanced-datatable/css/demo_page.css",
          "admin/js/advanced-datatable/css/demo_table.css",
          "admin/js/data-tables/DT_bootstrap.css",
          "admin/js/jvector-map/jquery-jvectormap-1.2.2.css",
          "admin/css/clndr.css",
          "admin/js/css3clock/css/style.css",
          "admin/js/morris-chart/morris.css",
          "admin/js/jquery-ui/jquery-ui-1.10.1.custom.min.css",
          "admin/js/select2/select2.css",
          "admin/css/style.css",
          "admin/css/style-responsive.css",
          ]
        }
      }
    },

  });

  // Plugins
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-jshint');

  // Tasks
  grunt.registerTask('default_admin', ['uglify', 'cssmin']);

};
