<?php
include 'header.php';
?>
<!--main content start-->
<section id="main-content" class="">
  <section class="wrapper">
    <!-- page start-->
    <div class="row">
      <div class="col-md-12">
        <div data-collapsed="0" class="panel">
          <div class="panel-body">
            <div class="row">
              <div id="ajaxreturn"></div>
              <form class="ajax">
                <input name="ajax" type="hidden" value="article_add">
                <input name="post[type]" type="hidden" value="article">
                <input name="post[type]" type="hidden" value="article">
                <input name="post[status]" type="hidden" value="active">
                <div class="col-md-12 form-group">
                  <input name="post[title]" type="text" placeholder="Título" class="form-control">
                </div>
                <div class="col-xs-12 form-group">
                  <textarea class="form-control ckeditor" name="post[content]" rows="6"></textarea>
                </div>
                <div class="col-xs-12 form-group">
                  <textarea class="form-control ckeditor" name="post[excerpt]" rows="6"></textarea>
                </div>
                <!-- <div id="upload" class="col-xs-12 form-group" style="padding: 0 15px 0 15px;margin-bottom: 0;">
<div id="drop" style="padding:20px;">
Arraste as imagens para cá
<h3 style="font-weight:900;">OU</h3>
<a>Procure no seu computador</a>
<input type="file" name="upl" multiple />
</div>
<ul style="padding: 0 0 15px 0;">
<!-- The file uploads will be shown here --
</ul>
</div> -->
                <div class="col-md-12 form-group">
                  <div id="recept-thumbnail" style="margin-bottom:15px;"></div>
                  <a class="btn btn-primary btn-large col-xs-12" href="#thumbnail" data-toggle="modal">Selecionar Destaque</a>
                  <input type="hidden" name="post[thumbnail_id]" id="recept-thumbnail-id">
                </div>
                <div class="col-md-12 form-group">
                  <!-- <input name="post[thumbnail_id]" type="text" placeholder="Thumbnail ID" class="form-control"> -->
                </div>
                <div class="col-xs-12 form-group icheck">
                  <?php
$list_categories_1 = "";
$list_categories_2 = "";
$list_categories_3 = "";
$i = 1;
foreach ( FG1_Categories::query( "article_cats" ) as $category ) {
  switch ( $i ) {
    case 1:
    $list_categories_1 .= "
                                <div class='square single-row'>
                                  <div class='checkbox '>
                                    <input name='post[categories][" . $category['id'] . "]' type='checkbox' >
                                    <label>" . $category['title'] . "</label>
                                  </div>
                                </div>
                              ";
    break;
    case 2:
    $list_categories_2 .= "
                                <div class='square single-row'>
                                  <div class='checkbox '>
                                    <input name='post[categories][" . $category['id'] . "]' type='checkbox' >
                                    <label>" . $category['title'] . "</label>
                                  </div>
                                </div>
                              ";
    break;
    case 3:
    $list_categories_3 .= "
                                <div class='square single-row'>
                                  <div class='checkbox '>
                                    <input name='post[categories][" . $category['id'] . "]' type='checkbox' >
                                    <label>" . $category['title'] . "</label>
                                  </div>
                                </div>
                              ";
    $i = 0;
    break;
  }
  $i++;
}
                  ?>
                  <div class="col-md-4 col-xs-12 no-padding">
                    <?php echo $list_categories_1; ?>
                  </div>
                  <div class="col-md-4 col-xs-12 no-padding">
                    <?php echo $list_categories_2; ?>
                  </div>
                  <div class="col-md-4 col-xs-12 no-padding">
                    <?php echo $list_categories_3; ?>
                  </div>
                </div>
                <div class="col-xs-12 form-group">
                  <label class=" col-md-2 control-label">Tags</label>
                  <div class="col-md-10">
                    <input name="post[tags]" id="tags" type="text" class="tags" value="php,ios,javascript,ruby,android,kindle" />
                  </div>
                </div>
                <div class="col-xs-12 form-group">
                  <button class="btn btn-default btn-large col-xs-12" type="submit">Enviar</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
      <!-- page end-->
      </section>
  </section>
  <!--main content end-->
</section>
<?php
include 'footer.php';
echo FG1_Media::mediaModal( "thumbnail", FALSE, "image", "recept-thumbnail", $site_url );
?>
<script>
  $(function() {
    $('#tags').tagsInput({width:'auto'});
    // Uncomment this line to see the callback functions in action
    //      $('input.tags').tagsInput({onAddTag:onAddTag,onRemoveTag:onRemoveTag,onChange: onChangeTag});
    // Uncomment this line to see an input with no interface for adding new tags.
    //      $('input.tags').tagsInput({interactive:false});
  });
</script>
<style>
  .no-padding{padding:0;}.icheck div{margin:0}.icheck .checkbox label{margin-left:10px;}.media-gal .item.selected{padding: 5px;border: 6px solid #34A6F9;background-color: rgba(52, 166, 249, 0.32);}.media-gal .item.selected .fa{display: block !important;}#recept-thumbnail:after{content:' ';display: block; clear: both;}
</style>
