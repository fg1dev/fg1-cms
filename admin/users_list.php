<?php
  include 'header.php';
?>
      <!--main content start-->
      <section id="main-content">
        <section class="wrapper">
          <div class="row">
            <div class="col-md-12">

              <div data-collapsed="0" class="panel">

                <div class="panel-body">

                    <table  class="display table table-bordered table-striped" id="dynamic-table">
                      <thead>
                        <tr>
                          <th>Nome</th>
                          <th>Email</th>
                          <th>Data de Cadastro</th>
                          <th>Posição</th>
                          <th>Excluir</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                          foreach ( FG1_Users::query( "", "", $user[0]['role_id'] ) as $user ) {
                            ?>
                            <tr onclick="window.location='users_edit.php?user_id=<?php echo $user['id']; ?>';" style='cursor: pointer;'>
                              <td><?php echo $user['first_name'] . " " . $user['last_name']; ?></td>
                              <td><?php echo $user['mail'] ?></td>
                              <td><?php echo date( "d/m/Y", strtotime( $user['registered'] ) ); ?></td>
                              <td>Role</td>
                              <td align="center"><i class="fa fa-remove"></i></td>
                            </tr>
                            <?php
                          }
                        ?>
                      </tbody>
                      <tfoot>
                        <tr>
                          <th>Nome</th>
                          <th>Email</th>
                          <th>Data de Cadastro</th>
                          <th>Posição</th>
                          <th>Excluir</th>
                        </tr>
                      </tfoot>
                    </table>
                  </div>

                </div>
              </div>
            </div>
          </div>
        </section>
      </section>
      <!--main content end-->
    </section>
<?php
  include 'footer.php';
?>
