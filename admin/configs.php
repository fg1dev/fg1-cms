<?php
  include 'header.php';
?>
      <!--main content start-->
      <section id="main-content" class="">
        <section class="wrapper">
          <!-- page start-->

          <div class="row">
            <div class="col-md-8 col-md-offset-2 col-xs-12">

              <div data-collapsed="0" class="panel">

                <div class="panel-body">

                  <div class="row">
                    <div id="ajaxreturn"></div>

                    <form class="ajax">
                    <input name="ajax" type="hidden" value="site_options">

                    <div class="col-md-12 form-group">
                      <input type="text" placeholder="Nome do Site" class="form-control" name="post[site_name]" value="<?php echo FG1_SiteOptions::query( 'site_name' )[0]['value']; ?>">
                    </div>

                    <div class="col-md-12 form-group">
                      <input type="text" placeholder="Email para contato" class="form-control" name="post[contact_mail]" value="<?php echo FG1_SiteOptions::query( 'contact_mail' )[0]['value']; ?>">
                    </div>

                    <div class="col-md-12 form-group">
                      <textarea placeholder="Código Google Analytics" class="form-control" style="max-width:100%;min-width: 100%;" name="post[ga_code]"><?php echo FG1_SiteOptions::query( 'ga_code' )[0]['value']; ?></textarea>
                    </div>

                    <div class="col-md-12 form-group">
                      <input type="text" placeholder="Logo do Site" class="form-control" name="post[site_logo]" value="<?php echo FG1_SiteOptions::query( 'site_logo' )[0]['value']; ?>">
                    </div>
                    <!-- <div id="upload" class="col-xs-12 form-group" style="padding: 0 15px 0 15px;margin-bottom: 0;">
                      <div id="drop" style="padding:20px;">
                        Arraste as imagens para cá
                        <h3 style="font-weight:900;">OU</h3>
                        <a>Procure no seu computador</a>
                        <input type="file" name="upl" multiple />
                      </div>

                      <ul style="padding: 0 0 15px 0;">
                          <!-- The file uploads will be shown here --
                      </ul>
                    </div> -->
                    <div class="col-md-12 form-group">
                      <button class="btn btn-default btn-large col-xs-12" type="submit">Enviar</button>
                    </div>
                    </form>

                </div>

              </div>

            </div>
          </div>


          <!-- page end-->
        </section>
      </section>
      <!--main content end-->
    </section>
<?php
  include 'footer.php';
?>
