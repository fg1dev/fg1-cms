<?php
include 'header.php';
?>
<!--main content start-->
<section id="main-content">
  <section class="wrapper">
    <!--mini statistics start-->
    <div class="row">
      <div class="col-md-4">
        <section class="panel">
          <div class="panel-body">
            <div class="top-stats-panel">
              <div class="gauge-canvas">
                <h4 class="widget-h">Visitantes neste Momento</h4>
                <canvas width=160 height=100 id="gauge"></canvas>
              </div>
              <ul class="gauge-meta clearfix">
                <li id="gauge-textfield" class="pull-left gauge-value"></li>
                <li class="pull-right gauge-title">Safe</li>
              </ul>
            </div>
          </div>
        </section>
      </div>
      <div class="col-md-4">
        <section class="panel" style="background-color: #9972b5 !important;">
          <div class="panel-body">
            <div class="monthly-stats pink" style="padding: 27px;">
              <div class="clearfix">
                <h4 style="text-align:center; width: 100%;" class="pull-left">Visitantes este Mês</h4>
              </div>
              <div class="sparkline" data-type="line" data-resize="true" data-height="75" data-width="90%" data-line-width="1" data-min-spot-color="false" data-max-spot-color="false" data-line-color="#ffffff" data-spot-color="#ffffff" data-fill-color="" data-highlight-line-color="#ffffff" data-highlight-spot-color="#e1b8ff" data-spot-radius="3" data-data="[100,200,459,234,600,800,345,987,675,457,765,100,200,459,234,600,800,345,987,675,457,765]"></div>
            </div>
            </section>
          </div>
        <div class="col-md-4">
          <div class="profile-nav alt">
            <section class="panel">
              <div class="user-heading alt clock-row terques-bg clock-time">
                <h1><span id="day"></span> de <span id="month"></span></h1>
                <p class="text-left"><span id="year"></span>, <span id="week-day"></span></p>
                <p class="text-left"><span id="hours"></span>:<span id="mins"></span></p>
              </div>
              <ul id="clock">
                <li id="sec"></li>
                <li id="hour"></li>
                <li id="min"></li>
              </ul>
            </section>
          </div>
        </div>
      </div>
      <!--mini statistics end-->
      <!--mini statistics start-->
      <div class="row">
        <div class="col-md-3">
          <div class="mini-stat clearfix">
            <span class="mini-stat-icon orange"><i class="fa fa-shopping-cart"></i></span>
            <div class="mini-stat-info">
              <span>320</span>
              Novos Pedidos
            </div>
          </div>
        </div>
        <div class="col-md-3">
          <div class="mini-stat clearfix">
            <span class="mini-stat-icon tar"><i class="fa fa-rocket"></i></span>
            <div class="mini-stat-info">
              <span>22,450</span>
              Produtos Cadastrados
            </div>
          </div>
        </div>
        <div class="col-md-3">
          <div class="mini-stat clearfix">
            <span class="mini-stat-icon pink"><i class="fa fa-money"></i></span>
            <div class="mini-stat-info">
              <span>34,320</span>
              Em vendas HOJE
            </div>
          </div>
        </div>
        <div class="col-md-3">
          <div class="mini-stat clearfix">
            <span class="mini-stat-icon green"><i class="fa fa-eye"></i></span>
            <div class="mini-stat-info">
              <span>3233</span>
              Visitantes ÚNICOS
            </div>
          </div>
        </div>
      </div>
      <!--mini statistics end-->
      <div class="row">
        <div class="col-md-6">
          <div class="event-calendar clearfix panel">
            <div class="col-xs-12 calendar-block">
              <div class="cal1">
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <!--widget weather start-->
          <div class="weather-widget clearfix">
            <div class="pull-left weather-icon">
              <canvas id="<?php echo FG1_SiteOptions::skycon(); ?>" width="60" height="60"></canvas>
            </div>
            <div>
              <ul class="weather-info">
                <li class="weather-city"><?php echo FG1_SiteOptions::user_Location()->city . " - " . FG1_SiteOptions::user_Location()->region; ?><i class="ico-location"></i></li>
                <li class="weather-cent"><span><?php echo round( 5 / 9 * ( FG1_SiteOptions::weather()->query->results->channel->item->condition->temp - 32 ) ); ?></span></li>
                <li class="weather-status"><?php echo FG1_SiteOptions::weather_Desc_pt(); ?></li>
              </ul>
            </div>
          </div>
          <!--widget weather end-->
        </div>
      </div>
      </section>
  </section>
  <!--main content end-->
</section>
<?php
include 'footer.php';
?>
<script src="js/gauge/gauge.js"></script>
<script src="js/dashboard.js"></script>
<!--script for this page-->
<style>#clock {margin: -166px auto 0;padding-bottom: 190px;}.profile-nav.alt .panel{background: #1fb5ad;}.weather-widget:last-child, .weather-widget:first-child{padding: 11px 15px;}.weather-widget{padding: 10px 15px;}</style>
