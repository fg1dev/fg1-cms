<?php
include 'header.php';
?>
<!--main content start-->
<section id="main-content">
  <section class="wrapper">
    <div class="row">
      <div class="col-md-12">
        <div data-collapsed="0" class="panel">
          <div class="panel-body">
            <div class="row col-md-6 col-xs-12">
              <div id="ajaxreturn"></div>
              <h3>Adicionar Nova Categoria</h3>
              <form class="ajax">
                <input name="ajax" type="hidden" value="category">
                <input name="post[type]" type="hidden" value="article_cats">
                <div class="col-md-12 form-group">
                  <input name="post[title]" type="text" placeholder="Título" class="form-control">
                </div>
                <div class="col-xs-12 form-group">
                  <textarea name="post[description]" class="form-control ckeditor" name="editor1" rows="6"></textarea>
                </div>
                <!-- <div id="upload" class="col-xs-12 form-group" style="padding: 0 15px 0 15px;margin-bottom: 0;">
<div id="drop" style="padding:20px;">
Arraste as imagens para cá
<h3 style="font-weight:900;">OU</h3>
<a>Procure no seu computador</a>
<input type="file" name="upl" multiple />
</div>
<ul style="padding: 0 0 15px 0;">
<!-- The file uploads will be shown here --
</ul>
</div> -->
                <div class="col-md-12 form-group">
                  <input name="post[thumbnail_id]" type="text" placeholder="Thumbnail ID" class="form-control">
                </div>
                <div class="col-xs-12 form-group">
                  <label class="col-md-1 col-xs-12 control-label" style="padding:0;">Parente</label>
                  <select id="e1" name="post[parent_id]" class="col-md-11 col-xs-12 populate" style="padding: 0;">
                    <option selected>Nenhum</option>
                    <?php
foreach (FG1_Categories::query( "", "", "no", 0 ) as $category) {
  echo "<option value='" . $category['id'] . "'>" . $category['title'] . "</option>";
  foreach (FG1_Categories::query( "", "", "yes", $category['id'] ) as $child_category) {
    echo "<option value='" . $child_category['id'] . "'>— " . $child_category['title'] . "</option>";
  }
}
                    ?>
                  </select>
                </div>
                <div class="col-xs-12 form-group">
                  <button class="btn btn-default btn-large col-xs-12" type="submit">Enviar</button>
                </div>
              </form>
            </div>
            <div class="row col-md-6 col-xs-12">
              <h3>Categorias</h3>
              <table  class="display table table-bordered table-striped" id="dynamic-table">
                <thead>
                  <tr>
                    <th>Nome</th>
                    <th>Slug</th>
                    <th>Nº Posts</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
foreach (FG1_Categories::query( "article_cats" ) as $category) {
                  ?>
                  <tr onclick="window.location='category_edit.php?category_id=<?php echo $category['id']; ?>';" style='cursor: pointer;'>
                    <td><?php echo $category['title']; ?></td>
                    <td><?php echo $category['slug']; ?></td>
                    <td>666</td>
                  </tr>
                  <?php
}
                  ?>
                </tbody>
                <tfoot>
                  <tr>
                    <th>Nome</th>
                    <th>Slug</th>
                    <th>Nº Posts</th>
                  </tr>
                </tfoot>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</section>
<!--main content end-->
</section>
<?php
include 'footer.php';
?>
