<?php
  include 'header.php';
?>
      <!--main content start-->
      <section id="main-content">
        <section class="wrapper">
          <!-- page start-->

          <div class="row">
            <div class="col-sm-12">
              <section class="panel">
                <div class="panel-body">
                  <div class="adv-table">
                    <table  class="display table table-bordered table-striped" id="dynamic-table">
                      <thead>
                        <tr>
                          <th>Título</th>
                          <th>Data</th>
                          <th>Nº Comentários</th>
                          <th class="hidden-phone">Categorias</th>
                          <th class="hidden-phone">Tags</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                          foreach ( fg1_Posts::query( "article" ) as $post ) {
                            ?>
                            <tr onclick="window.location='article_edit.php?post_id=<?php echo $post['id']; ?>';" style='cursor: pointer;'>
                              <td><?php echo $post['title']; ?></td>
                              <td><?php echo date( "d/m/Y", strtotime( $post['create_time'] ) ); ?></td>
                              <td>666</td>
                              <td>
                                <?php
                                  $i = 0;

                                  $len = count( fg1_Posts::categories( $post['id'] ) );

                                  foreach ( fg1_Posts::categories( $post['id'] ) as $category ) {

                                    echo ( $i == $len - 1 ? $category : $category . ", " );

                                    $i++;

                                  }
                                ?>
                              </td>
                              <td><?php echo $post['tags']; ?></td>
                            </tr>
                            <?php
                          }
                        ?>
                      </tbody>
                      <tfoot>
                        <tr>
                          <th>Título</th>
                          <th>Data</th>
                          <th>Nº Comentários</th>
                          <th class="hidden-phone">Categorias</th>
                          <th class="hidden-phone">Tags</th>
                        </tr>
                      </tfoot>
                    </table>
                  </div>
                </div>
              </section>
            </div>
          </div>

          <!-- page end-->
        </section>
      </section>
      <!--main content end-->
    </section>
<?php
  include 'footer.php';
?>
