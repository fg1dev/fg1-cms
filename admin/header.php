<?php
//error_reporting(0);
include '../includes/fg1_Loader.php';
FG1_Loader::fg1Classes();
$user_id = FG1_Users::isLogged();
if( $user_id === FALSE ){
  header("Location: login.php?login=0");
}
$site_url = FG1_SiteOptions::query( "site_url" );
$site_url = $site_url[0]['value'];
$user = fg1_Users::query( $user_id );
?>
<!DOCTYPE html>
<html lang="pt-br">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="ThemeBucket">
    <link rel="shortcut icon" href="images/favicon.png">
    <title>FG1 — ADMIN</title>

    <?php
    $styles = array( "css/application.min.css" );
    echo FG1_Loader::stylesheet( $styles );
    ?>

  </head>
  <body>
    <section id="container">
      <!--header start-->
      <header class="header fixed-top clearfix">
        <!--logo start-->
        <div class="brand">
          <a href="index.php" class="logo">
            <img class="col-xs-12" src="images/fg1_colors.png" alt="">
          </a>
          <div class="sidebar-toggle-box hidden-lg hidden-md">
            <div class="fa fa-bars"></div>
          </div>
        </div>
        <!--logo end-->
        <div class="top-nav clearfix">
          <!-- user info start-->
          <ul class="nav pull-right top-menu">
            <!-- user login dropdown start-->
            <li class="dropdown">
              <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                <img alt="" src="images/gallery/image5.jpg">
                <span class="username"><?php echo $user[0]['first_name'] . " " . $user[0]['last_name']; ?></span>
                <b class="caret"></b>
              </a>
              <ul class="dropdown-menu extended logout">
                <li><a href="users_profile.php"><i class=" fa fa-suitcase"></i>Seu Perfil</a></li>
                <li><a href="configs.php"><i class="fa fa-cog"></i>Configurações</a></li>
                <li><a href="login.php"><i class="fa fa-key"></i>Sair</a></li>
              </ul>
            </li>
            <!-- user login dropdown end -->
          </ul>
          <!-- user info end-->
        </div>
      </header>
      <!--header end-->
      <!--sidebar start-->
      <aside>
        <div id="sidebar" class="nav-collapse">
          <!-- sidebar menu start-->
          <div class="leftside-navigation">
            <ul class="sidebar-menu" id="nav-accordion">
              <?php
              $args = array(
                'Painel de Controle' => array(
                  'icon' => 'fa-dashboard',
                  'link' => 'index.php',
                ),
                'Mídia' => array(
                  'icon' => 'fa-picture-o',
                  'sub-menu' => array(
                    'Ver Todas' => 'media_list.php',
                    'Adicionar Nova' => 'media_add.php',
                  ),
                ),
                'Artigos' => array(
                  'icon' => 'fa-file-text-o',
                  'sub-menu' => array(
                    'Ver Todos' => 'article_list.php',
                    'Adicionar Novo' => 'article_add.php',
                    'Categorias' => 'article_categories.php',
                  ),
                ),
                'Usuários' => array(
                  'icon' => 'fa-users',
                  'sub-menu' => array(
                    'Ver Todos' => 'users_list.php',
                    'Adicionar Novo' => 'users_add.php',
                    'Seu Perfil' => 'users_profile.php',
                  ),
                ),
                'Comentários' => array(
                  'icon' => 'fa-comment-o',
                  'link' => 'comments.php',
                ),
                'Configurações' => array(
                  'icon' => 'fa-gears',
                  'link' => 'configs.php',
                ),
              );

              echo FG1_Loader::adminMenuList( $args );

              ?>
            </ul>
          </div>
          <!-- sidebar menu end-->
        </div>
      </aside>
      <!--sidebar end-->
