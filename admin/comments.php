<?php
  include 'header.php';
?>
      <!--main content start-->
      <section id="main-content">
        <section class="wrapper">
          <div class="row">
            <div class="col-md-12">

              <div data-collapsed="0" class="panel">

                <div class="panel-body">

                    <table  class="display table table-bordered table-striped" id="dynamic-table">
                      <thead>
                        <tr>
                          <th>Autor</th>
                          <th>Email</th>
                          <th>Assunto</th>
                          <th>Artigo</th>
                          <th>Data</th>
                          <th>IP</th>
                          <th>Bloquear</th>
                          <th>Aceitar</th>
                          <th>Excluir</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>Lorem Ipsum</td>
                          <td>loremipsum@mail.com</td>
                          <td>Consectur Maus</td>
                          <td>Dolor Sit Amet</td>
                          <td>17/01/2015</td>
                          <td>666.666.666.666</td>
                          <td align="center"><i class="fa fa-ban"></i></td>
                          <td align="center"><i class="fa fa-check"></i></td>
                          <td align="center"><i class="fa fa-remove"></i></td>
                        </tr>
                        <tr>
                          <td>Lorem Ipsum</td>
                          <td>loremipsum@mail.com</td>
                          <td>Consectur Maus</td>
                          <td>Dolor Sit Amet</td>
                          <td>11/01/2015</td>
                          <td>666.666.666.666</td>
                          <td align="center"><i class="fa fa-ban"></i></td>
                          <td align="center"><i class="fa fa-check"></i></td>
                          <td align="center"><i class="fa fa-remove"></i></td>
                        </tr>
                        <tr>
                          <td>Lorem Ipsum</td>
                          <td>loremipsum@mail.com</td>
                          <td>Consectur Maus</td>
                          <td>Dolor Sit Amet</td>
                          <td>07/01/2015</td>
                          <td>666.666.666.666</td>
                          <td align="center"><i class="fa fa-ban"></i></td>
                          <td align="center"><i class="fa fa-check"></i></td>
                          <td align="center"><i class="fa fa-remove"></i></td>
                        </tr>
                        <tr>
                          <td>Lorem Ipsum</td>
                          <td>loremipsum@mail.com</td>
                          <td>Consectur Maus</td>
                          <td>Dolor Sit Amet</td>
                          <td>11/01/2015</td>
                          <td>666.666.666.666</td>
                          <td align="center"><i class="fa fa-ban"></i></td>
                          <td align="center"><i class="fa fa-check"></i></td>
                          <td align="center"><i class="fa fa-remove"></i></td>
                        </tr>
                        <tr>
                          <td>Lorem Ipsum</td>
                          <td>loremipsum@mail.com</td>
                          <td>Consectur Maus</td>
                          <td>Dolor Sit Amet</td>
                          <td>09/01/2015</td>
                          <td>666.666.666.666</td>
                          <td align="center"><i class="fa fa-ban"></i></td>
                          <td align="center"><i class="fa fa-check"></i></td>
                          <td align="center"><i class="fa fa-remove"></i></td>
                        </tr>
                        <tr>
                          <td>Lorem Ipsum</td>
                          <td>loremipsum@mail.com</td>
                          <td>Consectur Maus</td>
                          <td>Dolor Sit Amet</td>
                          <td>11/01/2015</td>
                          <td>666.666.666.666</td>
                          <td align="center"><i class="fa fa-ban"></i></td>
                          <td align="center"><i class="fa fa-check"></i></td>
                          <td align="center"><i class="fa fa-remove"></i></td>
                        </tr>
                        <tr>
                          <td>Lorem Ipsum</td>
                          <td>loremipsum@mail.com</td>
                          <td>Consectur Maus</td>
                          <td>Dolor Sit Amet</td>
                          <td>16/01/2015</td>
                          <td>666.666.666.666</td>
                          <td align="center"><i class="fa fa-ban"></i></td>
                          <td align="center"><i class="fa fa-check"></i></td>
                          <td align="center"><i class="fa fa-remove"></i></td>
                        </tr>
                        <tr>
                          <td>Lorem Ipsum</td>
                          <td>loremipsum@mail.com</td>
                          <td>Consectur Maus</td>
                          <td>Dolor Sit Amet</td>
                          <td>11/01/2015</td>
                          <td>666.666.666.666</td>
                          <td align="center"><i class="fa fa-ban"></i></td>
                          <td align="center"><i class="fa fa-check"></i></td>
                          <td align="center"><i class="fa fa-remove"></i></td>
                        </tr>
                        <tr>
                          <td>Lorem Ipsum</td>
                          <td>loremipsum@mail.com</td>
                          <td>Consectur Maus</td>
                          <td>Dolor Sit Amet</td>
                          <td>11/01/2015</td>
                          <td>666.666.666.666</td>
                          <td align="center"><i class="fa fa-ban"></i></td>
                          <td align="center"><i class="fa fa-check"></i></td>
                          <td align="center"><i class="fa fa-remove"></i></td>
                        </tr>
                        <tr>
                          <td>Lorem Ipsum</td>
                          <td>loremipsum@mail.com</td>
                          <td>Consectur Maus</td>
                          <td>Dolor Sit Amet</td>
                          <td>14/01/2015</td>
                          <td>666.666.666.666</td>
                          <td align="center"><i class="fa fa-ban"></i></td>
                          <td align="center"><i class="fa fa-check"></i></td>
                          <td align="center"><i class="fa fa-remove"></i></td>
                        </tr>
                        <tr>
                          <td>Lorem Ipsum</td>
                          <td>loremipsum@mail.com</td>
                          <td>Consectur Maus</td>
                          <td>Dolor Sit Amet</td>
                          <td>11/01/2015</td>
                          <td>666.666.666.666</td>
                          <td align="center"><i class="fa fa-ban"></i></td>
                          <td align="center"><i class="fa fa-check"></i></td>
                          <td align="center"><i class="fa fa-remove"></i></td>
                        </tr>
                        <tr>
                          <td>Lorem Ipsum</td>
                          <td>loremipsum@mail.com</td>
                          <td>Consectur Maus</td>
                          <td>Dolor Sit Amet</td>
                          <td>13/01/2015</td>
                          <td>666.666.666.666</td>
                          <td align="center"><i class="fa fa-ban"></i></td>
                          <td align="center"><i class="fa fa-check"></i></td>
                          <td align="center"><i class="fa fa-remove"></i></td>
                        </tr>
                      </tbody>
                      <tfoot>
                        <tr>
                          <th>Autor</th>
                          <th>Email</th>
                          <th>Assunto</th>
                          <th>Artigo</th>
                          <th>Data</th>
                          <th>IP</th>
                          <th>Bloquear</th>
                          <th>Aceitar</th>
                          <th>Excluir</th>
                        </tr>
                      </tfoot>
                    </table>
                  </div>

                </div>
              </div>
            </div>
          </div>
        </section>
      </section>
      <!--main content end-->
    </section>
<?php
  include 'footer.php';
?>