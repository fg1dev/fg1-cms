<?php
  include 'header.php';
?>
      <!--main content start-->
      <section id="main-content">
        <section class="wrapper">
          <!-- page start-->
          <div class="row">
            <div class="col-sm-12">
              <section class="panel">
                <div class="panel-body">
                  <ul id="filters" class="media-filter">
                    <li><a href="#" data-filter="*">Todas</a></li>
                    <li><a href="#" data-filter=".image">Imagens</a></li>
                    <li><a href="#" data-filter=".video">Vídeo</a></li>
                  </ul>
                  <div id="gallery" class="media-gal">
                    <?php
                      foreach (FG1_Media::query() as $media){
                        ?>
                          <div class="<?php echo FG1_Media::get_type( $media['permalink'] ); ?> item " >
                            <a href="#myModal" class="<?php echo FG1_Media::get_type( $media['permalink'] ); ?>" data-toggle="modal">
                              <?php
                                if ( FG1_Media::get_type( $media['permalink'] ) === "video" ) {
                                  ?>
                                  <span class="media"><video class="col-xs-12" muted loop autoplay style="padding: 0;height: 210px;">
                                  <source src="<?php echo $media['permalink']; ?>" type="video/<?php echo $media['extension']; ?>">
                                 Seu Navegador não suporta tal funcionalidade, por favor atualize-o.
                                  </video></span>
                                  <?php
                                }else{
                                  echo '<span class="media"><img src="' . $media["permalink"] . '" alt="" /></span>';
                                }
                              ?>
                            </a>
                            <p><?php echo $media['title']; ?></p>
                          </div>
                        <?php
                      }
                    ?>
                  </div>
                  <div class="col-md-12 text-center clearfix">
                    <ul class="pagination">
                      <li><a href="#">«</a></li>
                      <li><a href="#">1</a></li>
                      <li><a href="#">2</a></li>
                      <li><a href="#">3</a></li>
                      <li><a href="#">4</a></li>
                      <li><a href="#">5</a></li>
                      <li><a href="#">»</a></li>
                    </ul>
                  </div>
                  <!-- Modal -->
                  <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                          <h4 class="modal-title">Editar Mídia</h4>
                        </div>
                        <div class="modal-body row">
                          <div class="col-md-5 img-modal">
                            <div id="modal-media"><img src="images/gallery/image1.jpg" alt=""></div>
                            <a href="#" class="col-xs-12 btn btn-white btn-sm"><i class="fa fa-eye"></i>Visualizar tamanho completo</a>
                            <p class="mtop10" id="file-name"><strong>Nome do arquivo:</strong> <span>img01.jpg</span></p>
                            <p id="file-type"><strong>Tipo do arquivo:</strong> <span>jpg</span></p>
                            <p id="file-dimensions"><strong>Tamanho:</strong> <span>300x200</span></p>
                          </div>
                          <div class="col-md-7">
                            <div class="form-group">
                              <label>Titulo da imagem</label>
                              <input id="title" value="awesome image" class="form-control">
                            </div>
                            <div class="form-group">
                              <label>URL da imagem</label>
                              <input id="link" value="images/gallery/img01.jpg" class="form-control" disabled>
                            </div>
                            <div class="pull-right">
                              <button class="btn btn-danger" type="button">Deletar</button>
                              <button class="btn btn-primary" type="button">Salvar mudanças</button>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- modal -->
                </div>
              </section>
            </div>
          </div>
          <!-- page end-->
        </section>
      </section>
      <!--main content end-->
    </section>
<?php
  include 'footer.php';
?>

    <script type="text/javascript">
        $(function() {
            var $container = $('#gallery');
            $container.isotope({
                itemSelector: '.item',
                animationOptions: {
                    duration: 750,
                    easing: 'linear',
                    queue: false
                }
            });

            // filter items when filter link is clicked
            $('#filters a').click(function() {
                var selector = $(this).attr('data-filter');
                $container.isotope({filter: selector});
                return false;
            });
        });
    </script>
    <script>
      $(".item").on('click', function(e){
        e.preventDefault();
        var media;
        var title;
        var img = new Image();
        if ($(this).hasClass('video')) {
          media = $(this).find('span').clone().html();
          $("#file-type span").empty().text( "Vídeo" );
        }else{
          media = $(this).find('span').clone().html();
          $("#file-type span").empty().text( "Imagem" );
        };
        title = $(this).find('p').clone().html();
        $("#file-name span").empty().text(title);
        $("#modal-media").empty().html(media);
        if ($(this).hasClass('video')) {
          $("#link").val($("#modal-media video source").attr("src"));
          $("a.btn-white").attr("href", function(){return $("#modal-media video source").attr("src");});
          $("#modal-media video").css("height", "auto").css("margin-bottom", "10px");
          $("#file-dimensions").empty();
        }else{
          $("#link").val($("#modal-media img").attr("src"));
          $("a.btn-white").attr("href", function(){return $("#modal-media img").attr("src");});
          img.onload = function() {
            $("#file-dimensions span").empty().text(this.width + 'x' + this.height);
          }
          img.src = $("#link").val();
        };
        $("#title").val(title);
      });
    </script>