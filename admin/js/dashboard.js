(function ($) {
    "use strict";
    $(document).ready(function () {
        if ($.fn.plot) {

            var d1 = [
            [0, 10],
            [1, 20],
            [2, 33],
            [3, 24],
            [4, 45],
            [5, 96],
            [6, 47],
            [7, 18],
            [8, 11],
            [9, 13],
            [10, 21]

            ];
            var data = ([{
                label: "Too",
                data: d1,
                lines: {
                    show: true,
                    fill: true,
                    lineWidth: 2,
                    fillColor: {
                        colors: ["rgba(255,255,255,.1)", "rgba(160,220,220,.8)"]
                    }
                }
            }]);
            var options = {
                grid: {
                    backgroundColor: {
                        colors: ["#fff", "#fff"]
                    },
                    borderWidth: 0,
                    borderColor: "#f0f0f0",
                    margin: 0,
                    minBorderMargin: 0,
                    labelMargin: 20,
                    hoverable: true,
                    clickable: true
                },
                // Tooltip
                tooltip: true,
                tooltipOpts: {
                    content: "%s X: %x Y: %y",
                    shifts: {
                        x: -60,
                        y: 25
                    },
                    defaultTheme: false
                },

                legend: {
                    labelBoxBorderColor: "#ccc",
                    show: false,
                    noColumns: 0
                },
                series: {
                    stack: true,
                    shadowSize: 0,
                    highlightColor: 'rgba(30,120,120,.5)'

                },
                xaxis: {
                    tickLength: 0,
                    tickDecimals: 0,
                    show: true,
                    min: 2,

                    font: {

                        style: "normal",


                        color: "#666666"
                    }
                },
                yaxis: {
                    ticks: 3,
                    tickDecimals: 0,
                    show: true,
                    tickColor: "#f0f0f0",
                    font: {

                        style: "normal",


                        color: "#666666"
                    }
                },
                //        lines: {
                //            show: true,
                //            fill: true
                //
                //        },
                points: {
                    show: true,
                    radius: 2,
                    symbol: "circle"
                },
                colors: ["#87cfcb", "#48a9a7"]
            };
            var plot = $.plot($("#daily-visit-chart"), data, options);

        }
// call this from the developer console and you can control both instances
var calendars = {};

$(document).ready( function() {

    // assuming you've got the appropriate language files,
    // clndr will respect whatever moment's language is set to.
    // moment.lang('ru');

    // here's some magic to make sure the dates are happening this month.
    var thisMonth = moment().format('MM-YYYY');

    // the order of the click handlers is predictable.
    // direct click action callbacks come first: click, nextMonth, previousMonth, nextYear, previousYear, or today.
    // then onMonthChange (if the month changed).
    // finally onYearChange (if the year changed).

    var clndr1 = $('.cal1').clndr({
        daysOfTheWeek: ['D', 'S', 'T', 'Q', 'Q', 'S', 'S'],
        // constraints: {
        //   startDate: '2013-11-01',
        //   endDate: '2013-11-15'
        // },
        multiDayEvents: {
            startDate: 'startDate',
            endDate: 'endDate'
        },
        showAdjacentMonths: true,
        adjacentDaysChangeMonth: false
    });

    // bind both clndrs to the left and right arrow keys
    $(document).keydown( function(e) {
        if(e.keyCode == 37) {
            // left arrow
            clndr1.back();
        }
        if(e.keyCode == 39) {
            // right arrow
            clndr1.forward();
        }
    });

});

});


})(jQuery);


if (Skycons) {
    /*==Weather==*/
    var skycons = new Skycons({
        "color": "#aec785"
    });
    // on Android, a nasty hack is needed: {"resizeClear": true}
    // you can add a canvas by it's ID...
    skycons.add("clear-day", Skycons.CLEAR_DAY);
    skycons.add("clear-night", Skycons.CLEAR_NIGHT);
    skycons.add("partly-cloudy-day", Skycons.PARTLY_CLOUDY_DAY);
    skycons.add("partly-cloudy-night", Skycons.PARTLY_CLOUDY_NIGHT);
    skycons.add("cloudy", Skycons.CLOUDY);
    skycons.add("rain", Skycons.RAIN);
    skycons.add("sleet", Skycons.SLEET);
    skycons.add("snow", Skycons.SNOW);
    skycons.add("wind", Skycons.WIND);
    skycons.add("fog", Skycons.FOG);
    // start animation!
    skycons.play();
    // you can also halt animation with skycons.pause()
    // want to change the icon? no problem:
    skycons.set("clear-day", Skycons.CLEAR_DAY);
    skycons.set("clear-night", Skycons.CLEAR_NIGHT);
    skycons.set("partly-cloudy-day", Skycons.PARTLY_CLOUDY_DAY);
    skycons.set("partly-cloudy-night", Skycons.PARTLY_CLOUDY_NIGHT);
    skycons.set("cloudy", Skycons.CLOUDY);
    skycons.set("rain", Skycons.RAIN);
    skycons.set("sleet", Skycons.SLEET);
    skycons.set("snow", Skycons.SNOW);
    skycons.set("wind", Skycons.WIND);
    skycons.set("fog", Skycons.FOG);

}

if (Gauge) {
    /*Knob*/
    var opts = {
        lines: 12, // The number of lines to draw
        angle: 0, // The length of each line
        lineWidth: 0.48, // The line thickness
        pointer: {
            length: 0.6, // The radius of the inner circle
            strokeWidth: 0.03, // The rotation offset
            color: '#464646' // Fill color
        },
        limitMax: 'true', // If true, the pointer will not go past the end of the gauge
        colorStart: '#fa8564', // Colors
        colorStop: '#fa8564', // just experiment with them
        strokeColor: '#F1F1F1', // to see which ones work best for you
        generateGradient: true
    };


    var target = document.getElementById('gauge'); // your canvas element
    var gauge = new Gauge(target).setOptions(opts); // create sexy gauge!
    gauge.maxValue = 3000; // set max gauge value
    gauge.animationSpeed = 32; // set animation speed (32 is default value)
    gauge.set(1150); // set actual value
    gauge.setTextField(document.getElementById("gauge-textfield"));

}
