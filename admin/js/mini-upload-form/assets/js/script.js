$.imgReload = function() {
  var retries = 0;
  var loaded = 1;
  $("img").each(function() {
    if (!this.complete || typeof this.naturalWidth == "undefined" || this.naturalWidth == 0) {
      var src = $(this).attr("src");
      var date = new Date();
      $(this).attr("src", src + "?v=" + date.getTime()); //slightly change url to prevent loading from cache
      loaded =0;
    }
  });
  retries +=1;
  if(retries < 10){ //if after 10 retries error images are not fixed maybe because they are not present on server, the recursion will break the loop
    if(loaded == 0)
      {setTimeout('$.imgReload()',4000); // I think 4 seconds is enough to load a small image (<50k) from a slow server
    }
    //all images have been loaded
    else {// alert("images loaded");
    }
  }
  //if error images cannot be loaded  after 10 retries
  else {// alert("recursion exceeded");
  }
}
$(function(){

    var ul = $('#gallery');

    $('#drop a').click(function(){
        // Simulate a click on the file input button
        // to show the file browser dialog
        $(this).parent().find('input').click();
    });

    // Initialize the jQuery File Upload plugin
    $('#upload').fileupload({

        // This element will accept file drag/drop uploading
        dropZone: $('#drop'),

        // This function is called when a file is added to the queue;
        // either via the browse button, or via drag/drop:
        add: function (e, data) {
            console.log(data);
            img_id = "img_" + Math.random();
            var tpl = $('<div class="item" style="cursor:pointer;" data-imageid=""><i class="fa fa-check"style="position: absolute;font-size: 25px;color: #0020FF;margin: -5px; display:none;"></i><?php<span class="media"><img id="' + img_id + '" src="uploads/" alt="" /></span><p></p></div>');

            // Append the file name and file size
            tpl.find('p').text(data.files[0].name);
            tpl.find('img').attr("src", "/fg1_cms/uploads/" + data.files[0].name);
            //document.getElementById(img_id).src = "uploads/" + data.files[0].name;

            // Add the HTML to the UL element
            data.context = $(tpl).appendTo(ul);
            $.imgReload();

            // Initialize the knob plugin
            tpl.find('input').knob();

            // Listen for clicks on the cancel icon
            tpl.find('span').click(function(){

                if(tpl.hasClass('working')){
                    jqXHR.abort();
                }

                tpl.fadeOut(function(){
                    tpl.remove();
                });

            });

            // Automatically upload the file once it is added to the queue
            var jqXHR = data.submit();
        },

        progress: function(e, data){

            // Calculate the completion percentage of the upload
            var progress = parseInt(data.loaded / data.total * 100, 10);

            // Update the hidden input field and trigger a change
            // so that the jQuery knob plugin knows to update the dial
            data.context.find('input').val(progress).change();

            if(progress == 100){
                data.context.removeClass('working');
            }
        },

        fail:function(e, data){
            // Something has gone wrong!
            data.context.addClass('error');
        }

    });


    // Prevent the default action when a file is dropped on the window
    $('#drop').on('dragover', function (e) {
        e.preventDefault();
        e.stopPropagation();
        $("#drop").css( "background-color", "#34A6F9" );
        $("#drop").css( "border-color", "#0020FF" );
        $("#drop").css( "color", "#fff" );
    });
    $('#drop').on('drop dragleave', function (e) {
        e.preventDefault();
        e.stopPropagation();
        $("#drop").css( "background-color", "#fff" );
        $("#drop").css( "border-color", "#F1F2F7" );
        $("#drop").css( "color", "#000" );
    });

    // Helper function that formats the file sizes
    function formatFileSize(bytes) {
        if (typeof bytes !== 'number') {
            return '';
        }

        if (bytes >= 1000000000) {
            return (bytes / 1000000000).toFixed(2) + ' GB';
        }

        if (bytes >= 1000000) {
            return (bytes / 1000000).toFixed(2) + ' MB';
        }

        return (bytes / 1000).toFixed(2) + ' KB';
    }

});