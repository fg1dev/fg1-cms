$(function() {

    setInterval( function() {
        var seconds = new Date().getSeconds();
        var sdegree = seconds * 6;
        var srotate = "rotate(" + sdegree + "deg)";

        $("#sec").css({"-moz-transform" : srotate, "-webkit-transform" : srotate});

    }, 1000 );


    setInterval( function() {
        var year = new Date().getFullYear();
        $(".clock-time #year").empty().text( year );
        var day = new Date().getDate();
        $(".clock-time #day").empty().text( day );
        var month = new Date().getMonth();
        switch( month ){
            case 0:
                $(".clock-time #month").empty().text( "Janeiro" );
                break;
            case 1:
                $(".clock-time #month").empty().text( "Fevereiro" );
                break;
            case 2:
                $(".clock-time #month").empty().text( "Março" );
                break;
            case 3:
                $(".clock-time #month").empty().text( "Abril" );
                break;
            case 4:
                $(".clock-time #month").empty().text( "Maio" );
                break;
            case 5:
                $(".clock-time #month").empty().text( "Junho" );
                break;
            case 6:
                $(".clock-time #month").empty().text( "Julho" );
                break;
            case 7:
                $(".clock-time #month").empty().text( "Agosto" );
                break;
            case 8:
                $(".clock-time #month").empty().text( "Setembro" );
                break;
            case 9:
                $(".clock-time #month").empty().text( "Outubro" );
                break;
            case 10:
                $(".clock-time #month").empty().text( "Novembro" );
                break;
            case 11:
                $(".clock-time #month").empty().text( "Dezembro" );
                break;
        }
        var weekday = new Date().getDay();
        switch( weekday ){
            case 0:
                $(".clock-time #week-day").empty().text( "Domingo" );
                break;
            case 1:
                $(".clock-time #week-day").empty().text( "Segunda Feira" );
                break;
            case 2:
                $(".clock-time #week-day").empty().text( "Terça Feira" );
                break;
            case 3:
                $(".clock-time #week-day").empty().text( "Quarta Feira" );
                break;
            case 4:
                $(".clock-time #week-day").empty().text( "Quinta Feira" );
                break;
            case 5:
                $(".clock-time #week-day").empty().text( "Sexta Feira" );
                break;
            case 6:
                $(".clock-time #week-day").empty().text( "Sábado" );
                break;
        };
        var hours = new Date().getHours();
        $(".clock-time #hours").empty().text( hours );
        var mins = new Date().getMinutes();
        if ( mins.toString().length == 1 ) {
            $(".clock-time #mins").empty().text( "0" + mins );
        }else{
            $(".clock-time #mins").empty().text( mins );
        };
        var hdegree = hours * 30 + (mins / 2);
        var hrotate = "rotate(" + hdegree + "deg)";

        $("#hour").css({"-moz-transform" : hrotate, "-webkit-transform" : hrotate});

    }, 1000 );


    setInterval( function() {
        var mins = new Date().getMinutes();
        var mdegree = mins * 6;
        var mrotate = "rotate(" + mdegree + "deg)";

        $("#min").css({"-moz-transform" : mrotate, "-webkit-transform" : mrotate});

    }, 1000 );

});