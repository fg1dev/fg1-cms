$(document).ready(function (e) {
  $(".ajax").on('submit',function(e) {
    e.preventDefault();
    for ( instance in CKEDITOR.instances ){
      CKEDITOR.instances[instance].updateElement();
    }
    $.ajax({
      url: "ajax.php",
      type: "POST",
      data:  new FormData(this),
      contentType: false,
      cache: false,
      processData:false,
      success: function(data)
      {
        console.log(data);
        $(this).trigger("reset");
        $("#ajaxreturn").html("Post adicionado com Sucesso!");
      },
      error: function(data)
      {
        console.log(data);
        $("#ajaxreturn").html("Ocorreu um erro contate o administrador do sistema =(");
      }
    });
  });
});
