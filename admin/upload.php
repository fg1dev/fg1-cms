<?php
include '../includes/fg1_Loader.php';
FG1_Loader::fg1Classes();
// A list of sizes to crop/resize files
$sizes = array(
  0 => array( 150, 150 ),
  1 => array( 350, 350 ),
  2 => array( 650, 650 ),
);
// A list of permitted file extensions
$allowed = array('png', 'jpg', 'gif', 'mp4', 'avi', 'wmv', 'mp3');
$files = array();
$i = 0;
foreach( $_FILES as $file ){
  $files['files'][$i] = FG1_Media::upload( $file, $allowed, $sizes );
  $i++;
}
echo json_encode( $files );
