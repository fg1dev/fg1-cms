<?php
  include 'header.php';
  $category = FG1_Categories::query( "", $_GET['category_id'] );
?>
      <!--main content start-->
      <section id="main-content">
        <section class="wrapper">
          <div class="row">
            <div class="col-md-8 col-md-offset-2 col-xs-12">

              <div data-collapsed="0" class="panel">

                <div class="panel-body">

                  <div class="row col-xs-12">

                    <div id="ajaxreturn"></div>

                    <form class="ajax">

                    <input name="ajax" type="hidden" value="category_edit">

                    <input name="post[category_id]" type="hidden" placeholder="Título" class="form-control" value="<?php echo $_GET['category_id']; ?>">

                    <div class="col-md-12 form-group">
                      <input name="post[title]" type="text" placeholder="Título" class="form-control" value="<?php echo $category[0]['title']; ?>">
                    </div>

                    <div class="col-xs-12 form-group">
                      <textarea name="post[description]" class="form-control ckeditor" name="editor1" rows="6"><?php echo $category[0]['description']; ?></textarea>
                    </div>

                    <!-- <div id="upload" class="col-xs-12 form-group" style="padding: 0 15px 0 15px;margin-bottom: 0;">
                      <div id="drop" style="padding:20px;">
                        Arraste as imagens para cá
                        <h3 style="font-weight:900;">OU</h3>
                        <a>Procure no seu computador</a>
                        <input type="file" name="upl" multiple />
                      </div>

                      <ul style="padding: 0 0 15px 0;">
                        <!-- The file uploads will be shown here --
                      </ul>
                    </div> -->

                    <div class="col-md-12 form-group">
                      <input name="post[thumbnail_id]" type="text" placeholder="Thumbnail ID" class="form-control" value="<?php echo $category[0]['thumbnail_id']; ?>">
                    </div>

                    <div class="col-xs-12 form-group">
                      <label class="col-md-1 col-xs-12 control-label" style="padding:0;">Parente</label>
                      <select id="e1" name="post[parent_id]" class="col-md-11 col-xs-12 populate" style="padding: 0;">
                        <option selected>Nenhum</option>
                        <?php

                          foreach (FG1_Categories::query( "", "", "no", 0 ) as $main_category) {

                            if ( $_GET['category_id'] !== $main_category['id'] ) {

                              echo "<option value='" . $main_category['id'] . "' " . ( $category[0]['parent'] === $main_category['id'] ? "selected" :"" ) . ">" . $main_category['title'] . "</option>";

                            }

                          }

                        ?>
                      </select>
                    </div>

                    <div class="col-xs-12 form-group">
                      <button class="btn btn-default btn-large col-xs-12" type="submit">Enviar</button>
                    </div>
                    </form>

                  </div>

                </div>
              </div>
            </div>
          </div>
        </section>
      </section>
      <!--main content end-->
    </section>
<?php
  include 'footer.php';
?>
