<?php
include 'header.php';
?>
<!--main content start-->
<section id="main-content" class="">
  <section class="wrapper">
    <!-- page start-->

    <div class="row">
      <div class="col-md-8 col-md-offset-2 col-xs-12">

        <div data-collapsed="0" class="panel">

          <div class="panel-body">

            <div class="row">
              <div id="ajaxreturn"></div>

              <form class="ajax">

                <input name="ajax" type="hidden" value="user_add">

                <div class="col-md-12 form-group">
                  <input type="text" name="post[mail]" placeholder="Email" class="form-control">
                </div>

                <div class="col-md-12 form-group">
                  <input type="text" name="post[first_name]" placeholder="Primeiro Nome" class="form-control">
                </div>

                <div class="col-md-12 form-group">
                  <input type="text" name="post[last_name]" placeholder="Último Nome" class="form-control">
                </div>

                <div class="col-md-12 form-group">
                  <input type="password" name="post[password]" placeholder="Senha" class="form-control">
                </div>

                <div class="col-md-12 form-group">
                  <input type="password" name="post[re_password]" placeholder="Confirmação de Senha" class="form-control">
                </div>

                <!-- <div id="upload" class="col-xs-12 form-group" style="padding: 0 15px 0 15px;margin-bottom: 0;">
                <div id="drop" style="padding:20px;">
                Arraste as imagens para cá
                <h3 style="font-weight:900;">OU</h3>
                <a>Procure no seu computador</a>
                <input type="file" name="upl" multiple />
                </div>

                <ul style="padding: 0 0 15px 0;">
                <!-- The file uploads will be shown here --
                </ul>
                </div> -->

                <div class="col-md-12 form-group">
                  <input type="text" name="post[thumbnail_id]" placeholder="Foto do perfil" class="form-control">
                </div>

                <div class="col-xs-12 form-group">
                  <label class="col-md-1 col-xs-12 control-label" style="padding:0;">Função</label>
                  <select id="e1" name="post[role_id]" class="col-md-11 col-xs-12 populate" style="padding: 0;">
                    <?php
                  foreach ( FG1_Users::roles( $user[0]['role_id'] ) as $role ) {

                    echo "<option value='" . $role['id'] . "' " . ( $user[0]['role_id'] === $role['id'] ? "selected" :"" ) . ">" . $role['title'] . "</option>";

                  }

                    ?>
                  </select>
                </div>

                <div class="col-md-12 form-group">
                  <button class="btn btn-default btn-large col-xs-12" type="submit">Enviar</button>
                </div>

              </form>

            </div>

          </div>

        </div>
      </div>

      <!-- page end-->
      </section>
  </section>
  <!--main content end-->
</section>
<?php
include 'footer.php';
?>
<script>
  $(function() {

    $('#tags').tagsInput({width:'auto'});

    // Uncomment this line to see the callback functions in action
    //      $('input.tags').tagsInput({onAddTag:onAddTag,onRemoveTag:onRemoveTag,onChange: onChangeTag});

    // Uncomment this line to see an input with no interface for adding new tags.
    //      $('input.tags').tagsInput({interactive:false});
  });
</script>
<style>
  .no-padding{padding:0;}
  .icheck div{margin:0}
  .icheck .checkbox label{margin-left:10px;}
</style>
