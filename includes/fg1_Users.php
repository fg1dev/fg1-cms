<?php

include 'PHPMailer\PHPMailerAutoload.php';

class FG1_Users{

  static function insert( $post ){

    $mail = $post['mail'];

    $first_name = $post['first_name'];

    $last_name = $post['last_name'];

    $password = self::passwordCrypt( $post['password'] );

    $thumbnail_id = $post['thumbnail_id'];

    $role_id = $post['role_id'];

    $pdo =  new PDO( DB_TYPE . ":host=" . DB_HOST . ";dbname=" . DB_NAME, DB_USER, DB_USER_PSSWD);

    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $pdo->query("INSERT INTO " . DB_PREFIX . "fg1_users(login, password, first_name, last_name, photo_id, mail, role_id) VALUES ('" . $mail . "', '" . $password . "', '" . $first_name . "', '" . $last_name . "', '" . $thumbnail_id . "', '" . $mail . "', '" . $role_id . "')" );

  }

  static function update( $post ){

    $mail = $post['mail'];

    $first_name = $post['first_name'];

    $last_name = $post['last_name'];

    $password = self::passwordCrypt( $post['password'] );

    $thumbnail_id = $post['thumbnail_id'];

    $role_id = $post['role_id'];

    $pdo =  new PDO( DB_TYPE . ":host=" . DB_HOST . ";dbname=" . DB_NAME, DB_USER, DB_USER_PSSWD);

    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    if ( $post['password'] === "" ) {

      $pdo->query("UPDATE " . DB_PREFIX . "fg1_users SET first_name = '" . $first_name . "', last_name = '" . $last_name . "', photo_id = '" . $thumbnail_id . "', role_id = '" . $role_id . "' WHERE mail = '" . $mail . "'" );

    }else{

      $pdo->query("UPDATE " . DB_PREFIX . "fg1_users SET password = '" . $password . "', first_name = '" . $first_name . "', last_name = '" . $last_name . "', photo_id = '" . $thumbnail_id . "' WHERE mail = '" . $mail . "'" );

    }

  }

  static function passwordUpdate( $password, $mail ){

    $pdo =  new PDO( DB_TYPE . ":host=" . DB_HOST . ";dbname=" . DB_NAME, DB_USER, DB_USER_PSSWD);

    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $pdo->query("UPDATE " . DB_PREFIX . "fg1_users SET password = '" . $password . "' WHERE mail = '" . $mail . "'" );

  }

  static function query( $id = "", $mail = "", $role_id = "" ){

    $pdo =  new PDO( DB_TYPE . ":host=" . DB_HOST . ";dbname=" . DB_NAME, DB_USER, DB_USER_PSSWD);

    if ( $id !== "" ) {

      return $pdo->query("SELECT * FROM " . DB_PREFIX . "fg1_users WHERE id = '" . $id . "'")->fetchAll();

    }elseif( $mail !== "" ){

      return $pdo->query("SELECT * FROM " . DB_PREFIX . "fg1_users WHERE mail = '" . $mail . "'")->fetchAll();

    }elseif( $role_id !== "" ){

      $get_value = $pdo->prepare("SELECT value FROM " . DB_PREFIX . "fg1_roles WHERE id = '" . $role_id . "'");

      $get_value->execute();

      foreach( $get_value->fetchAll() as $role_val ){

        $roles = $pdo->prepare("SELECT id FROM " . DB_PREFIX . "fg1_roles WHERE value <= " . $role_val['value'] . "");

        $roles->execute();

        $roles_query = "";

        $i = 0;

        foreach( $roles->fetch(PDO::FETCH_ASSOC) as $role ){

          if( $i === 0 ){

            $roles_query .= "" . $role;

          }else{

            $roles_query .= ", " . $role;

          }

          $i++;

        }

      }

      return $pdo->query("SELECT * FROM " . DB_PREFIX . "fg1_users WHERE role_id IN (" . $roles_query . ")")->fetchAll();

    }else{

      return $pdo->query("SELECT * FROM " . DB_PREFIX . "fg1_users")->fetchAll();

    }

  }

  static function login( $login, $password, $expire = 99999999 ){

    $pdo =  new PDO( DB_TYPE . ":host=" . DB_HOST . ";dbname=" . DB_NAME, DB_USER, DB_USER_PSSWD);

    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $password = self::passwordCrypt( $password );

    $check_user = $pdo->query("SELECT id, login, password FROM " . DB_PREFIX . "fg1_users WHERE login = '" . $login . "' AND password = '" . $password . "'")->fetchAll();

    if ( !empty( $check_user ) ) {

      foreach ( $check_user as $user ){

        session_cache_expire( $expire );

        session_start();

        $_SESSION['user'] = $user['id'];

        session_write_close();

      }

      return TRUE;

    }else{

      return FALSE;

    }

  }

  private function passwordCrypt( $password ){

    $options = [
      'cost' => 15,
      'salt' => hash_hmac( "SHA512", "FG1Comunicacao", "FG1", false ),
    ];

    return password_hash( $password, PASSWORD_BCRYPT, $options );

  }

  static function isLogged(){

    session_start();

    if (!empty($_SESSION['user'])) {

      return $_SESSION['user'];

    }

    session_write_close();

    return FALSE;

  }

  static function randomPassword( $length ){

    $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%&";

    return substr( str_shuffle( $chars ), 0, $length );

  }

  static function forgot( $mail ){

    $pdo =  new PDO( DB_TYPE . ":host=" . DB_HOST . ";dbname=" . DB_NAME, DB_USER, DB_USER_PSSWD);

    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $validate_mail = $pdo->prepare("SELECT login FROM " . DB_PREFIX . "fg1_users WHERE mail = '" . $mail . "'");

    if( $validate_mail->execute() ){

      $password = self::randomPassword( 15 );

      self::passwordUpdate( self::passwordCrypt( $password ), $mail );

      $body_mail = "<div>A sua senha temporária é:" . $password . "</div>";
      $body_mail .= "<div>Caso não tenha sido você quem requisitou a alteração de senha não se preocupe, nosso sistema o protegeu. :D</div>";
      $mail = new PHPMailer;

      //$mail->SMTPDebug = 3;                               // Enable verbose debug output

      $mail->isSMTP();                                  // Set mailer to use SMTP
      $mail->CharSet = 'UTF-8';
      $mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
      $mail->SMTPAuth = true;                               // Enable SMTP authentication
      $mail->Username = 'back@fg1.com.br';                 // SMTP username
      $mail->Password = 'matheus15';                           // SMTP password
      $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
      $mail->Port = 587;                                    // TCP port to connect to

      $mail->From = 'senhas@fg1.com.br';
      $mail->FromName = 'FG1 Senhas';
      $mail->addAddress('back@fg1.com.br', 'Back FG1');     // Add a recipient

      $mail->isHTML(true);                                  // Set email format to HTML

      $mail->Subject = 'Senha Perdida';
      $mail->Body    = 'Foi solicitada uma senha temporária para sua conta:' . $body_mail;

      $mail->send();

      return TRUE;

    }else{

      return TRUE;

    }

  }

  static function loggout(){

    session_start();

    unset( $_SESSION['user'] );

    session_write_close();

  }

  static function roles( $role_id = "" ){

    $pdo =  new PDO( DB_TYPE . ":host=" . DB_HOST . ";dbname=" . DB_NAME, DB_USER, DB_USER_PSSWD);

    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    if( $role_id !== "" ){

      $get_value = $pdo->prepare("SELECT value FROM " . DB_PREFIX . "fg1_roles WHERE id = '" . $role_id . "'");

    }else{

      $get_value = $pdo->prepare("SELECT value FROM " . DB_PREFIX . "fg1_roles");

    }

    $get_value->execute();

    $roles_array = array();

    foreach( $get_value->fetchAll() as $role_val ){

      $roles = $pdo->query("SELECT id, title FROM " . DB_PREFIX . "fg1_roles WHERE value <= " . $role_val['value'] . "")->fetchAll();

      return $roles;

    }

  }

}
