<?php

class FG1_Ajax{

  static function execute( $data ){

    $ajax = $data['ajax'];

    $post = $data['post'];

    switch( $ajax ){

      case "article_add":

        echo FG1_Posts::insert( $post );

      break;

      case "article_edit":

        echo FG1_Posts::update( $post );

      break;

      case "category":

        echo FG1_Categories::insert( $post );;

      break;

      case "category_edit":

        echo FG1_Categories::update( $post );;

      break;

      case "user_add":

        echo FG1_Users::insert( $post );

      break;

      case "user_edit":

        echo FG1_Users::update( $post );

      break;

      case "site_options":

        echo FG1_SiteOptions::insertUpdate( $post );

      break;

    }

  }

}
