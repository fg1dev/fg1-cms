<?php

class FG1_Posts{

  static function insert( $post ){

    $type = $post['type'];

    $title = $post['title'];

    $content = $post['content'];

    $excerpt = $post['excerpt'];

    $tags = $post['tags'];

    $categories = $post['categories'];

    $meta = $post['meta'];

    $thumbnail_id = $post['thumbnail_id'];

    $permalink = strtolower( $title );

    $permalink = str_replace( " ", "-", $permalink );

    $status = $post['status'];

    try{

      $pdo =  new PDO( DB_TYPE . ":host=" . DB_HOST . ";dbname=" . DB_NAME, DB_USER, DB_USER_PSSWD);

      $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

      $stmt = $pdo->query("INSERT INTO " . DB_PREFIX . "fg1_posts(permalink, type, title, content, excerpt, tags, thumbnail_id, status) VALUES ('" . $permalink . "', '" . $type . "', '" . $title . "', '" . $content . "', '" . $excerpt . "', '" . $tags . "', '" . $thumbnail_id . "', '" . $status . "')" );

      $post_id = $pdo->lastInsertId();

      if ( !empty( $categories ) ) {

        foreach ($categories as $category_id => $value) {

          $pdo->query("INSERT INTO " . DB_PREFIX . "fg1_post_categories( post_id, category_id ) VALUES ('" . $post_id . "', '" . $category_id . "')" );

        }

      }

      if ( !empty( $meta ) ) {

        foreach ( $meta as $meta_key => $meta_value ) {

          $pdo->query("INSERT INTO " . DB_PREFIX . "fg1_postmeta( post_id, name, value ) VALUES ('" . $post_id . "', '" . $meta_key . "', '" . $meta_value . "')" );

        }

      }

      return "Artigo adicionado com sucesso!";

    }catch( exception $e ){

      return $e->getMessage();

    }

  }

  static function update( $post ){

    $post_id = $post['post_id'];

    $title = $post['title'];

    $content = $post['content'];

    $excerpt = $post['excerpt'];

    $tags = $post['tags'];

    $categories = $post['categories'];

    $meta = $post['meta'];

    $thumbnail_id = $post['thumbnail_id'];

    $status = $post['status'];

    $permalink = strtolower( $title );

    $permalink = str_replace( " ", "-", $permalink );

    try{

      $pdo =  new PDO( DB_TYPE . ":host=" . DB_HOST . ";dbname=" . DB_NAME, DB_USER, DB_USER_PSSWD);

      $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

      $stmt = $pdo->query("UPDATE " . DB_PREFIX . "fg1_posts SET permalink = '" . $permalink . "', title = '" . $title . "', content = '" . $content . "', excerpt = '" . $excerpt . "', tags = '" . $tags . "', thumbnail_id = '" . $thumbnail_id . "', status = '" . $status . "' WHERE id=" . $post_id . "" );

      if ( !empty( $categories ) ) {

        $pdo->query("DELETE FROM " . DB_PREFIX . "fg1_post_categories WHERE post_id = " . $post_id );

        foreach ($categories as $category_id => $value) {

          $pdo->query("INSERT INTO " . DB_PREFIX . "fg1_post_categories( post_id, category_id ) VALUES ('" . $post_id . "', '" . $category_id . "')" );

        }

      }

      if ( !empty( $meta ) ) {

        $pdo->query("DELETE FROM " . DB_PREFIX . "fg1_postmeta WHERE post_id = " . $post_id );

        foreach ( $meta as $meta_key => $meta_value) {

          $pdo->query("INSERT INTO " . DB_PREFIX . "fg1_postmeta( post_id, name, value ) VALUES ('" . $post_id . "', '" . $meta_key . "', '" . $meta_value . "')" );

        }

      }

      return "Artigo editado com sucesso!";

    }catch( exception $e ){

      return $e->getMessage();

    }

  }

  static function query( $type = "", $id = "" ){

    $pdo =  new PDO( DB_TYPE . ":host=" . DB_HOST . ";dbname=" . DB_NAME, DB_USER, DB_USER_PSSWD);

    if ( $type !== "" ) {

      return $pdo->query("SELECT * FROM " . DB_PREFIX . "fg1_posts WHERE type = '" . $type . "'")->fetchAll();

    }elseif( $id !== "" ){

      return $pdo->query("SELECT * FROM " . DB_PREFIX . "fg1_posts WHERE id = " . $id . "")->fetchAll();

    }else{

      return $pdo->query("SELECT * FROM " . DB_PREFIX . "fg1_posts")->fetchAll();

    }

  }

  static function categories( $post_id ){

    $pdo =  new PDO( DB_TYPE . ":host=" . DB_HOST . ";dbname=" . DB_NAME, DB_USER, DB_USER_PSSWD);

    $get_categories = $pdo->query("SELECT category_id FROM " . DB_PREFIX . "fg1_post_categories WHERE post_id = '" . $post_id . "'")->fetchAll( PDO::FETCH_ASSOC );

    $categories = array();

    foreach ($get_categories as $key => $value) {

      $categories[] = $value['category_id'];

    }

    return $categories;

  }

}
