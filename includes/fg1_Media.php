<?php

include 'wide-image/WideImage.php';

class FG1_Media extends WideImage{

  static function insert( $name, $extension ){

    $pdo =  new PDO( DB_TYPE . ":host=" . DB_HOST . ";dbname=" . DB_NAME, DB_USER, DB_USER_PSSWD);

    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $pdo->query("INSERT INTO " . DB_PREFIX . "fg1_media(permalink, title, extension) VALUES ('uploads/" . $name ."', '" . str_replace("." . $extension, "", $name) . "', '" . $extension . "')" );

    return $pdo->lastInsertId();

  }

  static function upload( $files, $allowed, $sizes ){

    try{

      $return = array();

      if(isset($files) && $files['error'] == 0){

        $extension = pathinfo($files['name'], PATHINFO_EXTENSION);

        if(!in_array(strtolower($extension), $allowed)){
          $return = array("status" => "error", "message" => "unsupported extension");
        }
        if ( file_exists( '../uploads/'.$files['name'] ) ) {
          $file_name = date( "H-i-s-" ) . $files['name'];
        }else{
          $file_name = $files['name'];
        }
        if(move_uploaded_file($files['tmp_name'], '../uploads/'.$file_name)){
          $mime_type = explode("/", $files['type']);
          $mime_type = $mime_type[0];
          if ($mime_type === 'image') {
            foreach ($sizes as $size) {
              $rsz_img = WideImage::load('../uploads/' . $file_name);
              $rsz_img->resize($size[0], $size[1], 'outside')->crop('center', 'middle', $size[0], $size[1])->saveToFile('../uploads/' . $size[0] . 'x' . $size[1] . '_' . $file_name);
            }
          }elseif ($mime_type === "video") {
            $video = $files['name'];
            $image_name = str_replace($extension, "", $file_name) . ".jpg";
          }
          $img_save = self::insert( $file_name, $extension );
          $img_src = self::query( $img_save );
          $return = array("status" => "success", "image_id" => $img_save, "image_src" => $img_src[0]['permalink']);
        }
      }else{
        $return = array("status" => "error", "message" => "Oops, an error ocurred, please contact administrator =(");
      }

      return $return;

    }catch( exception $e ){
      return $e->getMessage();
    }

    //return '{"status":"error":"general"}';

  }

  static function query( $id = NULL ){

    $pdo =  new PDO( DB_TYPE . ":host=" . DB_HOST . ";dbname=" . DB_NAME, DB_USER, DB_USER_PSSWD);

    if ( $id !== NULL ) {

      return $pdo->query("SELECT * FROM " . DB_PREFIX . "fg1_media WHERE id = '" . $id . "'")->fetchAll();

    }else{

      return $pdo->query("SELECT * FROM " . DB_PREFIX . "fg1_media ORDER BY id DESC")->fetchAll();

    }

  }

  static function get_type( $file ){

    $file_info = new finfo(FILEINFO_MIME);

    $mime_type = $file_info->buffer(file_get_contents($file));

    $mime_type = explode("/", $mime_type);

    $mime_type = $mime_type[0];

    switch ( $mime_type ) {
      case 'image':
      return "image";
      break;
      case 'video':
      return "video";
      break;

      default:
      # code...
      break;
    }

  }
  static function mediaModal( $id, $multiple = FALSE, $type = "", $imagesdiv , $site_url ){

    ob_start();

?>

<!-- Modal -->
<div class="modal fade col-xs-12 <?php echo ( $multiple === TRUE ? "multiple" : "" ); ?>" id="<?php echo $id; ?>" tabindex="-1" role="dialog" aria-labelledby="<?php echo $id; ?>Label" aria-hidden="true" style="overflow: hidden;height: 95%;margin-top: 1%;">
  <div class="modal-content col-xs-12" style="height:100%;position: absolute;width: 97%;margin-left: 0.5%;">
    <div class="modal-header col-xs-12">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <h4 class="modal-title">Selecionar Mídia</h4>
    </div>
    <div class="modal-body col-xs-12 row" style="overflow:auto;height:85%;">
      <form id="upload" method="post" action="upload.php" enctype="multipart/form-data">
        <div id="drop">
          Arraste as imagens para cá
          <h3 style="font-weight:900;">OU</h3>
          <a>Procure no seu computador</a>
          <input type="file" name="upl" multiple />
        </div>
        <ul></ul>
      </form>
      <div id="gallery" class="media-gal">
        <?php
    foreach (FG1_Media::query() as $media){
      if ( FG1_Media::get_type( $media['permalink'] ) !== "image" ) {
      }else{
        ?>
        <div class="<?php echo FG1_Media::get_type( $media['permalink'] ); ?> item " style="cursor:pointer;" data-imageid="<?php echo $media["id"]; ?>">
          <i class="fa fa-check"style="position: absolute;font-size: 25px;color: #0020FF;margin: -5px; display:none;"></i>
          <?php
        echo '<span class="media"><img id="srcfor-' . $media["id"] . '" src="' . $site_url . $media["permalink"] . '" alt="" /></span>';
          ?>
          <p><?php echo $media['title']; ?></p>
        </div>
        <?php
      }
    }
        ?>
      </div>
    </div>
    <div class="modal-footer col-xs-12">
      <button id="finish-<?php echo $id; ?>" type="button" class="btn btn-primary pull-right" aria-hidden="true">Concluir</button>
    </div>
  </div>
</div>
<!-- modal -->

<script>
  var images = [];
  var images_id = [];
  var clicks = 0;
  $("#<?php echo $id; ?> .media-gal .item").on("click", function(event){
    event.preventDefault();
    var image_id = $(this).data("imageid");
    var src = $("#srcfor-" + image_id).attr("src");
    if ($("#<?php echo $id; ?>.modal.fade").hasClass("multiple")) {
      if ($(this).hasClass("selected")) {
        $(this).removeClass("selected");
        images.splice( $.inArray(src, images) ,1 );
        images_id.splice( $.inArray(image_id,images_id) ,1 );
      }else{
        if (images.length == 0) {
          clicks = 0;
        };
        $(this).addClass("selected");
        images[clicks] = src;
        images_id[clicks] = image_id;
      }
    }else{
      if ($(this).hasClass("selected")) {
        $("#<?php echo $id; ?> .media-gal .item").removeClass("selected");
        images.splice( $.inArray(src, images) ,1 );
        images_id.splice( $.inArray(image_id,images_id) ,1 );
      }else{
        $("#<?php echo $id; ?> .media-gal .item").removeClass("selected");
        $(this).addClass("selected");
        images[0] = src;
        images_id[0] = image_id;
      }
    };
    clicks++;
  });
  $("#finish-<?php echo $id; ?>").on("click", function(event){
    event.preventDefault();
    var values = "";
    $("#<?php echo $imagesdiv; ?>").empty();
    images.forEach(function(img) {
      $("#<?php echo $imagesdiv; ?>").append("<img class='<?php echo ( $multiple ? 'col-xs-1' : 'col-xs-4 col-xs-offset-4' ); ?>' src='" + img + "'>");
    });
    images_id.forEach(function(id) {
      if (values !== "") {
        values = values + id + ",";
      }else{
        values = id + ",";
      };
    });
    $("#<?php echo $imagesdiv; ?>-id").val(values);
    $(".close").click();
  });
</script>

<?php

    $return = ob_get_clean();

    return $return;

  }

}
