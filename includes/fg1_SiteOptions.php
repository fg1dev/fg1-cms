<?php

  class FG1_SiteOptions{

    static function insertUpdate( $post ){

      try{

        $pdo =  new PDO( DB_TYPE . ":host=" . DB_HOST . ";dbname=" . DB_NAME, DB_USER, DB_USER_PSSWD);

        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        foreach ($post as $option_name => $option_values) {

          $pdo->query("INSERT INTO " . DB_PREFIX . "fg1_options(name, value) VALUES ('" . $option_name . "', '" . $option_values . "') ON DUPLICATE KEY UPDATE name='" . $option_name . "', value='" . $option_values . "'" );

        }

        return "Opções atualizadas com sucesso!";

      }catch( exception $e ){

        return $e->getMessage();

      }

    }

    static function query( $name ){

      $pdo =  new PDO( DB_TYPE . ":host=" . DB_HOST . ";dbname=" . DB_NAME, DB_USER, DB_USER_PSSWD);

      return $pdo->query("SELECT value FROM " . DB_PREFIX . "fg1_options WHERE name = '" . $name . "'")->fetchAll();

    }

    static function user_Location(){
      //$ip = $_SERVER['REMOTE_ADDR'];
      $ip = "187.95.108.175";

      $details = json_decode(file_get_contents("http://ipinfo.io/{$ip}"));

      return $details;

    }

    static function weather(){

      $BASE_URL = "http://query.yahooapis.com/v1/public/yql";

      $yql_query = 'select item.condition from weather.forecast where woeid in (select woeid from geo.places(1) where text="' . self::user_Location()->city . ', ' . self::user_Location()->region . '")';

      $yql_query_url = $BASE_URL . "?q=" . urlencode($yql_query) . "&u=c&format=json";

      // Make call with cURL
      $session = curl_init($yql_query_url);

      curl_setopt($session, CURLOPT_RETURNTRANSFER,true);

      $json = curl_exec($session);


      // Convert JSON to PHP object
      $phpObj =  json_decode($json);

      return $phpObj;

    }

    static function skycon(){

      $text = self::weather()->query->results->channel->item->condition->text;

      switch( $text ){

        case "Mostly Cloudy":
          if( date( "H" ) > 8 && date( "H" ) < 19 ){
            return "partly-cloudy-day";
          }else{
            return "partly-cloudy-night";
          }
          break;

        case "Cloudy":
            return "cloudy";
          break;

        case "Clear":
            return "clear-night";
          break;

        case "Sunny":
            return "clear-day";
          break;

        case "Snow":
            return "snow";
          break;

      }

    }

    static function weather_Desc_pt(){

      $text = self::weather()->query->results->channel->item->condition->text;

      switch( $text ){

        case "Mostly Cloudy":
          return "Parcialmnete Nublado";
          break;

        case "Cloudy":
            return "Nublado";
          break;

        case "Clear":
            return "Limpo";
          break;

        case "Sunny":
            return "Sol";
          break;

        case "Snow":
            return "Neve";
          break;

      }

    }

  }
