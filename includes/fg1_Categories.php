<?php

  class FG1_Categories{

    static function insert( $post ){

      $type = $post['type'];

      $title = $post['title'];

      $description = $post['description'];

      $thumbnail_id = $post['thumbnail_id'];

      $parent_id = $post['parent_id'];

      $slug = strtolower( $title );

      $slug = str_replace( " ", "-", $slug );

      try{

        $pdo =  new PDO( DB_TYPE . ":host=" . DB_HOST . ";dbname=" . DB_NAME, DB_USER, DB_USER_PSSWD);

        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $pdo->query("INSERT INTO " . DB_PREFIX . "fg1_categories(slug, type, title, description, thumbnail_id, parent) VALUES ('" . $slug . "', '" . $type . "', '" . $title . "', '" . $description . "', '" . $thumbnail_id . "', '" . $parent_id . "')" );

        return "Categoria adicionada com sucesso!";

      }catch( exception $e ){

        return $e->getMessage();

      }

    }


    static function update( $post ){

      $category_id = $post['category_id'];

      $title = $post['title'];

      $description = $post['description'];

      $thumbnail_id = $post['thumbnail_id'];

      $parent_id = $post['parent_id'];

      $slug = strtolower( $title );

      $slug = str_replace( " ", "-", $slug );

      try{

        $pdo =  new PDO( DB_TYPE . ":host=" . DB_HOST . ";dbname=" . DB_NAME, DB_USER, DB_USER_PSSWD);

        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $pdo->query("UPDATE " . DB_PREFIX . "fg1_categories SET slug = '" . $slug . "', title = '" . $title . "', description = '" . $description . "', thumbnail_id = '" . $thumbnail_id . "', parent = '" . $parent_id . "' WHERE id=" . $category_id );

        return "Categoria editada com sucesso!";

      }catch( exception $e ){

        return $e->getMessage();

      }

    }

    static function query( $type = "", $id = "", $children = "yes", $parent = 0 ){

      $pdo =  new PDO( DB_TYPE . ":host=" . DB_HOST . ";dbname=" . DB_NAME, DB_USER, DB_USER_PSSWD);

      $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

      if ( $type !== "" ) {

        return $pdo->query("SELECT * FROM " . DB_PREFIX . "fg1_categories WHERE type = '" . $type . "'")->fetchAll();

      }elseif( $id !== "" ){

        return $pdo->query("SELECT * FROM " . DB_PREFIX . "fg1_categories WHERE id = '" . $id . "'")->fetchAll();

      }elseif( $parent !== 0 ){

        return $pdo->query("SELECT * FROM " . DB_PREFIX . "fg1_categories WHERE parent = '" . $parent . "'")->fetchAll();

      }elseif( $children !== "yes" ){

        return $pdo->query("SELECT * FROM " . DB_PREFIX . "fg1_categories WHERE parent = '0'")->fetchAll();

      }else{

        return $pdo->query("SELECT * FROM " . DB_PREFIX . "fg1_categories")->fetchAll();

      }

    }

  }
