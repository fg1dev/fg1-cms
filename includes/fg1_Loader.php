<?php

  class FG1_Loader{

    static function config( $prefix, $extension ){

      $config_path = str_replace( "includes", "", dirname( __FILE__ ) );

      require $config_path . $prefix . 'config' . $extension;

    }

    static function fg1Classes(){

      $prefix = 'fg1_';

      $extension = '.php';

      self::config( $prefix, $extension );

      require $prefix . 'SiteOptions' . $extension;

      require $prefix . 'Media' . $extension;

      require $prefix . 'Categories' . $extension;

      require $prefix . 'Posts' . $extension;

      require $prefix . 'Users' . $extension;

      require $prefix . 'Ajax' . $extension;

    }

    static function stylesheet( $styles ){

      $return = "";

      foreach ($styles as $style) {

        $return .= "<link href='" . $style . "' rel='stylesheet'>";

      }

      return $return;

    }

    static function javascript( $scripts ){

      $return = "";

      foreach ($scripts as $script) {

        $return .= "<script src='" . $script . "'></script>";

      }

      return $return;

    }

    static function adminMenuList( $args ){

      foreach ($args as $menuItem => $values) {

        if ( array_key_exists( "sub-menu", $values ) ) {

          $active_main = ( array_search( basename($_SERVER['PHP_SELF']) , $values['sub-menu'] ) !== FALSE ? "active" : "" );

          ?>

          <li class="sub-menu">
            <a href="javascript:;" class=" dcjq-parent <?php echo $active_main; ?>">
              <i class="fa <?php echo $values['icon']; ?>"></i>
              <span><?php echo $menuItem; ?></span>
            </a>
            <ul class="sub">
              <?php

              foreach ($values['sub-menu'] as $subMenuItem => $subMenuItemValues) {

                $active_sub = (  basename($_SERVER['PHP_SELF']) === $subMenuItemValues ? "active" : "" );

                echo "<li class='" . $active_sub . "'><a href='" . $subMenuItemValues . "'>" . $subMenuItem . "</a></li>";

              }

              ?>
            </ul>
          </li>

          <?php

        }else{

          $active_main = ( array_search( basename($_SERVER['PHP_SELF']) , $values ) !== FALSE ? "active" : "" );

          ?>

          <li>
            <a href="<?php echo $values['link']; ?>" class='<?php echo $active_main; ?>'>
              <i class="fa <?php echo $values['icon']; ?>"></i>
              <span><?php echo $menuItem; ?></span>
            </a>
          </li>

          <?php

        }

      }

    }

  }
